## Preface

Hi there! My name is Sujit Prajapati and I completed my Master's of Science in Cloud Computing in 2022 from Munster Technological University, Cork, Ireland. As part of the coursework, I wrote my dissertation on "Model-Driven Network Configuration Management using NETCONF in NetDevOps". If you're interested in the thesis document, you can find it [here](https://drive.google.com/file/d/1hZ88412RrHZy7nDr62DVzvtsCaMwKIam/view). During my research, I realised that there is a lack of free resources on the web explaining the actual implementation of NetDevOps. In an attempt to share what I have learned, I am documenting my finding in this repository for anyone interested in the topic. I have intentionally omitted the NETCONF and YANG aspects from the dissertation here as I want to focus mainly on NetDevOps. I hope to write about those topics too in the future.

This is a good resource for anyone interested in Infrastructure Automation, Networking, NetDevOps, and DevOps. Even if you don't have a programming background I hope you'll find the concepts in the earlier chapters of this documentation interesting. I have tried my best to make it an interesing read. :slight_smile:

For the programmers out there, this repository contains a basic open source implementation of a NetDevOps workflow for you to play with. To follow along with the workflow code described in the later chapters, a basic understanding of Networking and Ansible is expected.

If there are any problems with running the code, you can create an [Issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#create-an-issue) in Gitlab. To help me identify any mistakes, give me suggestions, or just to have a chat about the topic, reach out to me over [LinkedIn](https://www.linkedin.com/in/pjptsujit/) or [Email](mailto:iamsujitprajapati@gmail.com?subject=GitLab%20NetDevOps%20Repo). I do not consider myself an expert in this topic by any means, but I am enthusiastic to learn and discuss.


[[_TOC_]]

## 1. Why am I reading this?

This repository will help you understand the need for NetDevOps, get familiar with its core principles, and provide an open source implementation of a NetDevOps workflow for you to play with.

## 2. Why NetDevOps?

In recent times, Computer Networking has been developing rapidly, resulting in significant advancements and innovation. Unfortunately, the domain of computer network management has not been able to keep up with this pace. Even today, the most prevalent techniques of configuring and managing networks are traditional methods of CLI (Command Line Interface) and SNMP (Simple Network Management Protocol). 

<img src="/Images/networking_vs_network_management.png" alt="Advancement of Networking vs Network Management" width="300"/>

Have you ever wondered why there still is a "[Culture of Fear](https://blogs.cisco.com/developer/embrace-netdevops-part-1)" around performing network changes? Based on a [survey](https://www.networkworld.com/article/3142838/top-reasons-for-network-downtime.html) conducted by Veriflow, a California based software company, 44% of the responders answered that network changes lead to outage or performance issues “several times a year.” Nearly all respondents (97%) agree that human error is a cause of network outages. Among respondents, 69% said they rely on manual processes, such as inspecting devices via the command line interface, inspecting configurations, and performing manual traceroutes or pings. 21% of respondents said it takes, on average, less than an hour to resolve networking issues and everyone else said it takes longer.

When it comes to network management and network changes, organizations operate with fear, as Network is the backbone of IT. All the other systems rely on the Network for communication, while the Network itself is very complex and fragile. The “if it isn’t broke, don’t fix it” mindset discourages frequent network changes. 

<img src="/Images/society.png" alt="Society" height="250"/>

Most of the errors during network changes occur due to human error, and the lack of testing and validation. There aren't any proper automated processes in place and the resolution time for network outages significantly affect the business. However, with a new wave of “programmable networks,” there is an abundance of possibilities to advance and improve network management. There is room for improvement to facilitate this new Networking age.

## 3. What is NetDevOps?

The domain of Networking has adopted the culture and strategies of Software Engineering into Network Engineering. This new practice of NetDevOps (a portmanteau of “Network,” “Development,” and “Operation”) promotes programmatic management of network configuration that is slowly being adopted as best practice in the industry.

<img src="/Images/netdevops_fusion.png" alt="NetDevOps Fusion" width="300"/>

In NetDevOps, small network changes are routine and expected. But this doesn't meant that the network changes are performed in a random manner without proper planning. It's the exact opposite! The configuration changes, testing, and deployment processes are well defined. This minimizes errors and provides a proper structure. We will further discuss the principles of NetDevOps in detail in upcoming chapters.


<img src="/Images/morpheus_switch_reboot.png" alt="Switch Reboot" height="250"/>

## 4.What is not NetDevOps?

We now know that NetDevOps borrows the core software development principles and practices and applies them to network configuration management.

<img src="/Images/netdevops_borrowing_from_devops.png" alt="Netdevops borrowing from DevOps" height="250"/>

We have discussed problems in the current landscape of network management, caused due to traditional, manual way of making network configuration changes. Surely automation can mitigate these issues! But what kind of automation qualifies as NetDevOps?

In many companies, engineers often write standalone scripts to automate their respective tasks. Even though such automation efforts are a positive step away from manual methods, it's not considered NetDevOps, it's only a part of the whole picture. Moreover, such scripts often don't follow any company wide standard conventions. This makes it difficult to keep track of automation efforts, and their effects on the network. The best practice is to follow the core principles of NetDevOps.

<img src="/Images/not_netdevops.png" alt="Not NetDevOps" width="300"/>

## 5. What are the core principles of NetDevOps?

In NetDevOps, the network configurations are stored, managed, and deployed using code rather than manual methods. The core concepts of NetDevOps are as follows:

<img src="/Images/netdevops_concepts.png" alt="NetDevOps Concepts" width="300"/>

### 5.1. Version Control

Similar to code in software engineering, the network configurations are stored in a central repository in a version-controlled manner.

### 5.2. Network as Code (NaC)

NetDevOps uses the concept of NaC (Network as Code), which is the application of IaC (Infrastructure as Code) specific to the networking domain. Rather than command line inputs, the network configurations are stored as code.

### 5.3. CI/CD Build Server

Developers merge their code into this central repository, followed by automated configuration, the theme of Continuous Integration (CI) and Continuous Delivery (CD). The deployments occur with the help of a programmatic interface eliminating any need for manual configuration.

### 5.4. Dev Environment

Unlike traditional networks, there is the provision for a separate development network to test changes before deploying to the production network. The separation of development and production networks plays a huge role in minimizing errors in NetDevOps.

### 5.5. Network Testing

After deployment, automated network tests are conducted to ensure nothing breaks after a change.

### 5.6. Alerts and Notification

The user is alerted with information regarding success or failure of the configuration push.

## 6. Design

Before implementation of the workflow, it is important to understand the design of a NetDevOps workflow. The example in this repository implements this design with a specific set of tools and technologies. However, the same design can be followed to have a more customised implementation with a desired technology stack.

<img src="/Images/explaining_complex_netdevops.png" alt="Explaining Comlpex NetDevOps" height="300"/>

### 6.1. Operational Requirements

The operational requirements of a NetDevOps system are listed below:

- Store and merge code-based network configurations in a version-controlled central repository. This mechanism is also known as Continuous Integration (CI).

- Automatically deploy configurations to the network devices by means of a "build server" when code changes are detected. The build server must have network connectivity to the devices. This mechanism is called Continuous Delivery (CD).

- Use a programmatic interface to configure network gears.

- Have separate development and production networks simulating identical topologies. The aim to catch any potential errors before the configuration is pushed to production.

- After every successful perform automated network tests based on test cases.

### 6.2. High Level Workflow Design

<img src="/Images/high_level_workflow_design.jpg" alt="High Level Project Workflow"/>

The Network Engineer makes configuration changes to the NaC files, and the change is registered by the Version Control Tool. The Build Server then detects the change and automatically triggers the pipeline to push the network configurations. A NaC tool with a programmatic interface to the devices is used to perform the configuration push. Prior to this, Network Engineer starts the dev (development) and prod (production) network topology simulation. However, it is to be noted that in a real life production scenario, there exists a live network. After the configuration change is done, the Network Testing component performs the tests based on pre-defined test cases. When the configurations are pushed to the dev network, tested, and no issues are detected, the same is done on the prod network.

### 6.3. Design Components

In terms of the design, the major system components in the workflow are as follows:

#### 6.3.1. Version Control

A version control tool tracks and manages the changes to the software code. A source control tool offers the features of branching and merging the code. Version control is a more general term than source control, and the two words are often used interchangeably. In NetDevOps, the changes to the network device configuration are updated in the central code repository and later pushed to the devices with the help of a build server. 

The central code repository used as the version control component in the system can manage the version of the NaC configurations and provide branching and merging features. Since there are different topologies for development (dev) and production (prod)
networks, the repository branches are crucial to pushing code to these branches separately. A “dev” branch is first used to push configurations to the dev network. After performing appropriate tests, the dev branch will be merged with the main branch to push configurations to the prod network. That covers the CI aspect of the requirement.

#### 6.3.2. Build Server

A server used to deploy configurations to the network devices is called a build server. A build server detects code changes and triggers the configured pipeline to push device configurations. 

In the context of this system, the build server must have network connectivity to the simulated network devices. The build server also uses pipelines with various stages for pushing code and testing the dev and prod network topologies. When any changes are committed to the central code repository, the changes are detected by the build server, it is triggered automatically, and the configuration changes are deployed to the network devices with the help of pipelines.

#### 6.3.3. Network as Code

The critical feature of network programmability is the adoption of NaC, where the network configurations are treated as code rather than manual commands to be run on the device CLI. NaC principles include storing network configurations in version control and deploying configurations with a programmatic interface. However, NaC here refers to the tools and technologies used to push configurations using a programmatic interface. 

There are various technologies available to implement NaC with network devices which translate to the device configuration. There are also multiple tools that help manage the inventory of network devices.

#### 6.3.4. Network Testing

Network Testing is a relatively new concept in the field of Network Engineering. It is a mechanism to determine the desired functioning of the network after performing a configuration change. Rather than using “ping” and looking out for open network issue tickets, network testing techniques can pre-emptively detect issues in the network after performing a configuration change. Based on pre-defined test cases, the state of the network can be accurately assessed to detect failures. The test cases are based on various parameters of the network that are to be checked.

#### 6.3.5. Network Simulation

Network Simulation is a technology used to replicate the functionalities of a hardware device using software code. Network Simulation tools can simulate and interface network devices when physical hardware is not available. Moreover, network virtualization has gained momentum in recent times to use network resources in software rather than hardware in the industry. In the case of a real-life scenario, the production network would be the actual physical network. Since there are cost issues managing a physical replica of the physical network, the development network can be simulated using software. 

The system explained must have separate dev and prod topologies which is a requirement of NetDevOps. In a real organisation, the prod network is the physical network and wont be simulated. When the system is deployed in a real organization, only the dev network is simulated. The topology can be a live network or a simulated one. This choice doesn’t affect the design of the system.

## 7. Implementation
The implementation of the open-source workflow in this repository uses a specific technology stack. As mentioned earlier, the design can be implemented with any tools and technologies that fulfill the purpose. 

<img src="/Images/design_vs_implementation.png" alt="Design vs Implementation" height="300"/>

### 7.1. Architecture

This project demonstrates a workflow that uses a NetDevOps environment to perform configuration changes on network devices. However, it isn't a good example of implementing network testing. Network testing is a relatively new concept and I have tried to share some of my findings about it to have a basic understanding of the concept.

Infrastructure as Code is the name given to the techniques used to describe and provision the compute, storage, network, and other resources as part of the deployment of the modern applications in cloud platforms. Network as Code is the application of the more generic IaC concept to the specific domain of networking. The network configurations are stored in a version-controlled manner in NaC and deployed to the network devices in a programmatic CI/CD fashion using a build server. Continuous Integration (CI) is the constant merging of development work with codebase so that automated testing can catch problems early. Continuous Delivery (CD) is a software package delivery mechanism for releasing code to staging for review and inspection. Continuous Deployment (CD) relies on CI and CD to automatically release code into production as soonas it is ready to facilitate a constant flow of new features into production. In this project, CD refers to the Continuous Delivery of the NaC configurations. The network is simulated to represent dev and prod topologies, and the network devices have a programmatic interface to make configuration changes.

### 7.2. Toolset

The various tools with their versions, used to realize the above architecture are noted below:

| Tool | Version |
| ------ | ------ |
| **Version Control** |
| Git | Version 2.25.1 | 
| GitLab | Public Instance |
| **Build Server** | 
| GitLab Runner | Version 14.3.2|
| **Network as Code** |
| Ansible | Version 2.13.6 | 
| Python | Version 3.8.10 |
| **Network Simulation** |
| GNS3 (Graphical Network Simulator-3) | Version 2.2.25 | 
| **Network Testing** |
| pyATS (Python Automation Test System) | Version 22.3 | 
| Python | Version 3.8.10 | 
| **Virtual Machine** |
| VMware Workstation 16 Player | Version 16.2.3 | 
| Ubuntu | Version 20.04 |

The various DevOps tools used are Git, GitLab, GitLab Runner, Ansible, GNS3 and python library pyATS. All the tools used in this project are open source.

They are described in detail below:

<img src="/Images/toolset.jpg" alt="Toolset"/>

#### 7.2.1. Git - Version Control

Version control, also known as source control, is the practice of tracking and managing changes to software code. Version control systems are software tools that help software teams work on modifications to source code over time. Git is a free and open-source distributed version control system designed to handle everything from small to massive projects with speed and efficiency.

There are several implementations of Git, such as GitLab and GitHub. Git is the most
widely accepted version control tool in the industry as it is reliable and provides excellent
performance compared to other version control tools.

#### 7.2.2.  GitLab with GitLab Runner - Version Control, CI/CD Build

GitLab provides an implementation of Git in a web-accessible portal where team members can collaborate. Git stores a copy of the NaC files on the local machine. But to submit and back up the work, the changes are pushed and backed up to the GitLab server.GitLab Server can be hosted on a developer’s machine, but this project uses the free public instance of GitLab for simplicity of setup.

CI/CD build servers are used to deploy and test proposed and deployed configurations. For this purpose, GitLab uses GitLab runner. GitLab Runner is an application that works with GitLab CI/CD to run jobs in a pipeline. GitLab CI/CD is used to run the pipelines necessary to push the network configurations in a NetDevOps fashion.

GitLab provides both source control and build systems. As it serves both these purposes, the need for a separate build system like Jenkins or Drone has been eliminated.

#### 7.2.3. Ansible - Network as Code

Ansible is a configuration management tool. Ansible is agent-less which means that no additional software must be installed on the target machines. Ansible is written in Python and uses Playbooks written in YAML (Yet Another Markup Language). In addition, it relies on SSH to connect to the managed hosts. And it depends on the host configuration file in which the hosts to be managed are placed. The network configurations are written and stored in a human-readable data-serialization language called YAML. It provides a rich library of modules to execute commands on devices from various vendors. Ansible playbooks contain a list of tasks that run when the playbook is executed.

Ansible is the most common automation tool among Network Engineers to make configuration changes to Network devices. Working as a Network Automation Engineer, I have gathered good experience with Ansible over other tools like Puppet or
NAPALM (Network Automation and Programmability Abstraction Layer with Multivendor support). Also, it is an open-source tool compared to something like Cisco NSO (Network Services Orchestrator), which is a Cisco Proprietary tool. Ansible is very easy to set up and use due to the agent-less nature of the tool.

#### 7.2.4. network_cli - Connection Protocol

Ansible supports multiple communication protocols selected for each network module which depend on the platform and the purpose of the module. The communication protocol is set using ansible connection variable while connecting to a device using Ansible. The communication protocols supported are shown below:

| Value of "ansible_connection" | Protocol |
| ------ | ------ |
| `ansible.netcommon.network_cli` | CLI over SSH | 
| `ansible.netcommon.netconf` | XML over SSH |
| `ansible.netcommon.httpapi` | API over HTTP/HTTPS |
| `local` | depends on provider |

### 7.3. Technical Setup

<img src="/Images/technical_set_up.jpg" alt="Technical Set Up"/>

All the tools used in this solution are free and open source. Regarding the technical setup, VMware Workstation Player runs on the local machine, which hosts the GNS3 and Ubuntu Linux VMs. GNS3 software runs on the GNS3 VM, and all the Git operations of configuration changes are done on the Ubuntu Linux VM.

The device configurations in this system are stored in version-controlled NaC; changes are made locally using Git and then pushed to the GitLab Server. Git has capabilities for team collaborations, but this project is implemented with a single user. The GitLab CI/CD is used to apply the configuration changes with Ansible and its NETCONF connection protocol and perform network tests with Python and pyATS.

As mentioned in the earlier section, the GitLab Runner runs on the Ubuntu Linux VM, and GitLab CI/CD uses it to run jobs in the pipeline. The GitLab CI/CD GUI indirectly applies configuration changes and performs tests, but it happens using the GitLab Runner in the background. In the GitLab GUI, it appears as though the pipelines are running on the GitLab server, but they run on the GitLab Runner. This project doesn’t provide an in-depth implementation of network testing but executes some preliminary functionalities to demonstrate network testing in NetDevOps. It uses a single test case
to test the interface errors on device interfaces.

The use of Ansible YAML files for inventory management, configuration management, and the communication protocol together make up the NaC aspect of this project. For this implementation, Ansible uses CLI protocol over SSH connection with the `ansible.netcommon.network_cli` value for ansible connection variable. The use of NETCONF protocol over SSH instead of CLI over SSH is a better alternative and I have done some study regarding this aspect. You can read more about it in my dissertation document mentioned earlier. However, since the focus of this repository is on NetDevOps, NETCONF will not be covered here. I may work on documenting NETCONF at a later time.

Both the dev and prod networks are simulated using GNS3 due to the unavailability of physical hardware. There isn’t a separate implementation for sending reports to the user in this project. The information on the GitLab GUI is adequate to relay all the essential information.

The following steps are involved in getting the tools to interact with each other and completing the environment’s set-up for this solution. It is to be noted that the steps may not be in exact order:

- Set up GNS3 and simulate the network devices.
- Create identical dev and test networks in GNS3.
- Manually enable the reachability to GitLab runner on GNS3 devices by configuring SSH and IP address.
- Create a new repository in GitLab Public Instance Server/ fork the current repository.
- Configure GitLab Runner to run on the local Ubuntu Linux VM and have the repository use this GitLab Runner as a build server.
- Write Ansible playbooks with network_cli connection protocol for configuration of the devices.
- Write network tests to be performed after pushing configurations onto devices.
- Create CI/CD pipelines in GitLab Server to push configurations and performs tests for the dev and prod networks.

### 7.4. Workflow

It is assumed that the devices aren’t out of the box, and SSH and IP address are configured. Also, the YAML configuration files are already written, and the engineer makes changes to them. The engineer first clones the repository onto the local machine using Git and makes desired changes to the YAML files. The changes are then pushed to the GitLab Server. The GitLab CI/CD pipeline is triggered when a version change is detected, and subsequent operations are run using GitLab Runner.

<img src="/Images/workflow.jpg" alt="Workflow"/>

In the CI/CD pipeline, GitLab Runner uses Ansible to push the configuration on the test network simulated in GNS3. The network tests define the desired topology and state of the network. If there is some problem in pushing the configuration (8a.), the pipeline fails, and the user is notified. If the configuration is applied successfully, but the network tests fail, the pipeline fails again, and the user is notified. In both these cases, the pipeline isn’t completed. But in case the pipeline for the test network succeeds, identical configurations and tests are performed on the prod network. If no manual changes are performed, the success of the configuration push in the dev network should imply the success of the configuration push in the prod network. That is because the dev network is an identical replica of the prod network. However, the network tests may succeed in dev but fail in prod, for example, when the prod devices have interface errors, but the dev devices don’t. The user is notified about all the pipeline operations in the GitLab GUI.

## 8. Setup

We finally come to the setup for the open source implementation of NetDevOps promised earlier!

<img src="/Images/it_s_time.gif" alt="It's time"/>

I have divided the setup into several subsections for ease of understanding. 

### 8.1. Ubuntu Virtual Machine

For this setup, we will use a virtual machine running on VMware Workstation hosting Ubuntu Version 20.04. Instructions for this setup are abundant on the internet and there are alternative tools that can be used to host a virtual machine. Hence, I don't think it's necessary to include any instructions here. 

### 8.2. GitLab and GitLab Runner

Initially, I had tried implementing the GitLab Server using Docker. But I faced some issues with the setup and finally opted for the public instance of GitLab. I think this option is relatively easier and it serves our purpose well. 

Initially we can [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) this project to your repository.

Ubuntu doesn't have GitLab Runner installed by default.

<img src="/Images/gitlab_runner_absent.png" alt="GitLab Runner Absent"/>

For setting up the GitLab Runner, we can first [install GitLab Runner](https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner) on the Ubuntu VM and [register the runner](https://docs.gitlab.com/runner/register/index.html#linux) to it with the instructions under **Settings > CI/CD > Runners > Specific runners** in the project. 

We can verify the installation with `gitlab-runner --version`

<img src="/Images/gitlab_runner_version.png" alt="GitLab Runner Version"/>

I have entered the tag as "sujit" for the registration. This value will be important in the pipeline later. Also we will select "shell" as the executor here.

<img src="/Images/gitlab_runner_registration.png" alt="GitLab Runner Registration"/>

### 8.3. Network Topology Simulation

As mentioned earlier, we will use GNS3 for this project. However this can be substituted with any other method of device simulation or even physical devices. The main requirement is to have separate and identical dev and prod topolgies. 

I followed this [video](https://www.youtube.com/watch?v=A0DEnMi09LY) by David Bombal to install GNS3. Now, the tricky part is getting images for GNS3. David Bombal instructs how to get paid Cisco images legally in this [video](https://youtu.be/jhh2_PP9JLU?t=150). However, there are also images available on the internet for free, which is not recommended by GNS3 and not somthing I can endorse. Personally, I could get my hands on the images I am using because of the organisation I am working in. So unfortunately obtaining images is something you'll have to figure out on your own.

We will a simple topology illustrated in the GNS3 UI below. The GNS3 UI also conveniently displays the various interfaces used to connect the GNS3 devices. The topology is designed to be divided into dev and prod networks, as discussed in the previous chapters. 

<img src="/Images/gns3_topology.png" alt="GNS3 topology"/>

The simulated device must initially have credentials set up and be configured to support SSH. The device simulated is Cisco IOS XE. "Switch1" is a layer 2 Ethernet Switch. "Cloud1" is used here to connect to the system’s physical interface and give DHCP (Dynamic Host Configuration Protocol) IP address to the network devices in the same subnet. Using the Cloud device, the device has the same IP subnet as the PC, which hosts the VMware Workstation. As the GitLab Runner hosted on VMware Workstation needs to have network reachability to the simulated devices, this is an appropriate feature in GNS3 to do so. As an example, the Ubuntu VM can ping the DHCP IP address of the Cisco IOS XE device in the dev network as shown below:.

<img src="/Images/ubuntu_ping.png" alt="Ubuntu Ping"/>
<img src="/Images/cisco_ip.png" alt="Cisco IP"/>

The following configuration is applied on the Cisco IOS XE Devices to enable SSH:

```
Router(config)#hostname CiscoIOSXE-Prod
CiscoIOSXE-Prod(config)#crypto key generate rsa
...
How many bits in the modulus [512]: 1024
...
CiscoIOSXE-Prod(config)#line vty 0 4
CiscoIOSXE-Prod(config-line)#login local
CiscoIOSXE-Prod(config-line)#transport input ssh
CiscoIOSXE-Prod(config-line)#transport output ssh
CiscoIOSXE-Prod(config-line)#exit
CiscoIOSXE-Prod(config)#username sujit privilege 15 password sujit
CiscoIOSXE-Prod(config)#enable password sujit
```

The following configuration is applied on the Cisco IOS XE Devices to obtain DCHP IP address:

```
CiscoIOSXE-Prod(config)#interface gigabitEthernet 1
CiscoIOSXE-Prod(config-if)#ip address dhcp
CiscoIOSXE-Prod(config-if)#no shutdown
CiscoIOSXE-Prod(config-if)#exit
```

### 8.4. Ansible

Ansible is used as the inventory and configuration management tool in this project. The instructions for installing Ansible can be found [here](https://adamtheautomator.com/install-ansible/).

In this project, we are only working on changing the hostname of the network devices. I mainly want to explain the workflow of NetDevOps and after understanding it, the same mechanism can be used for more complex configuration changes on network devices.

<img src="/Images/only_change_hostname.png" alt="Only Change Hostname"/>

The files and folder in this repository are described below:

| Name | Type | Purpose |
| ------ | ------ | ------ |
| group vars | Folder | A method to apply variables to multiple hosts in Ansible |
| network testing | Folder | Files for network testing |
| playbooks | Folder | Ansible playbooks |
| .gitlab-ci.yml | File | GitLab file to define CI/CD pipelines |
| ansible.cfg | File | Ansible configuration file |
| hosts-dev | File | Ansible inventory file for dev network |
| hosts-prod | File | Ansible inventory file for prod network |
| README.md | File | Generate the HTML summary at the bottom of projects in GitLab UI |

#### 8.4.1. Configuration File

The Ansible configuration file configures the behavior in which Ansible operates. The “ansible.cfg” acts as the configuration file for this project. The ansible.cfg acts as the configuration file for this project. The only parameter set is `host_key_checking` to False. The `known_hosts` file is a client file containing all remotely connected known hosts, and the ssh client uses this file. You can read more about it [here](https://linuxhint.com/known-hosts-file-ssh-linux/). If a host is reinstalled and has a different SSH key in `known_hosts`, this will result in an error message until corrected. If a host is not initially in `known_hosts` this will result in prompting for confirmation of the key. The `host_key_checking` variable determines whether this key should be checked or not. The value of `host_key_checking` is true by default.

#### 8.4.2. Inventory

Ansible runs against managed nodes or “hosts” using a list or group of lists known as “inventory.” Hosts are the devices against which the ansible playbooks are run. The inventory files used in this project are “hosts-dev” and “hosts-prod,” which correspond to the inventory of devices for the dev and prod networks respectively. The heading in brackets (`cisco-ios-xe`) is the group name that is used to group hosts and choose what groups and, in turn, hosts the playbooks should run against. The IP address here are the DHCP IP addresses of the devices in GNS3.

#### 8.4.3. Group Variables

[Group variables](https://www.educative.io/answers/what-are-groupvars-and-hostvars-in-ansible) are defined using “group_vars,” an Ansible-specific folder as part of the repository structure. It is a convenient way to apply variables to multiple hosts at once. The group variable files have the same name as the group names in the inventory files.

For example, “group_vars/cisco-ios-xe.yml” contains the group variables for that group. Even though it is a bad practice, and there are alternate ways to store credentials, the credentials are stored in the group variable files in this project for ease of execution and explanation.

In the playbook for Cisco IOS XE devices the value `hosts: cisco-ios-xe` corresponds to the hosts under the same group name in the inventory files explained earlier. It means the playbook is run against the hosts defined, also called target nodes.

#### 8.4.4. Playbooks

Ansible playbooks are blueprints written in YAML format that contain a set of tasks to be executed against the inventory defined. Ansible playbooks have the file extension “.yml” or “.yaml.” They are the aggregation of one or more “plays” in it. The playbooks are structured using plays where there can be one or more plays in a playbook. Ansible playbooks then contain [“tasks,”](https://www.digitalocean.com/community/tutorials/how-to-define-tasks-in-ansible-playbooks) which are the smallest unit of action that can be automated using Ansible playbooks. The playbooks are stored inside the “/playbooks” folder under the specific model of devices.

By default, playbooks gather [“facts”](https://www.redhat.com/sysadmin/playing-ansible-facts) which is data about the target nodes. A fact can be the IP address, BIOS information, a system’s software information, and even hardware information.

In the playbook "playbooks/cisco_ios_xe/hostname.yml" for Cisco IOS XE devices, there exists a single play named `Hostname change using network_cli`, and a single task named `Change hostname`. Since there is no requirement to gather facts from the target nodes, the value of gather facts is set to false.

Ansible [modules](https://docs.ansible.com/ansible/latest/module_plugin_guide/index.html) (also referred to as “task plugins” or “library plugins”) are units of code that can be used in a playbook task. The module used in this project is [cisco.ios.ios_config](https://docs.ansible.com/ansible/latest/collections/cisco/ios/ios_config_module.html#ansible-collections-cisco-ios-ios-config-module).
#### 8.4.4. Execution of playbooks

The ansible playbooks are executed using the command `ansible-playbook -i hosts-dev playbooks/cisco_ios_xe/hostname.yml` for Cisco IOS XE in the dev network. The `-i` flag indicates the inventory to run the playbook tasks against, and `playbooks/cisco_ios_xe/hostname.yml` indicates the path of the playbook. This command is run automatically by the NetDevOps CI/CD.

### 8.5. Network Testing

First and foremost, i wasn't able to implement network testing in the project CI/CD pipeline with GitLab Runner. And thios project doesn't go into depth about network testing. However, it's worth understanding the concepts of network testing and the role it plays in NetDevOps.

Network Testing is implemented using pyATS which provides an end-to-end test environment for developers to write test cases. [pyATS](https://pubhub.devnetcloud.com/media/pyats/docs/getting_started/index.html) is designed around the concept of testbeds where the devices under testing are described in YAML format. This YAML file does not have any relation with the Ansible YAML files.

Network Testing has a great potential for research since it’s in the early stages of adoption in the industry. This project’s scope does not include writing test cases and describing network testing in detail. This project uses an example test case available in [GitHub](https://github.com/hpreston/intro-network-tests) to test interface errors. There are several potential sources of [interface errors](https://netcraftsmen.com/understanding-interface-errors-and-tcp-performance/), including interface discards when there is insufficient bandwidth to support the traffic volume, misconfigured duplex and speed settings, excessive buffering on interfaces, and faulty cables or hardware. pyATS
was initially designed for Cisco’s internal engineering and supports a limited number of models of devices. In terms of this project, the testing is only implemented on the Cisco IOS XE devices with a single test case to check interface errors.

If you want to look into it, this [video](https://www.youtube.com/watch?v=tFeVdazq0O0) by Hank Preston is a good resource.

The files pertaining to network testing are contained inside the "network_testing" folder.

The three bare minimum components required for a network test project are:

#### 8.5.1. Testbed 

The testbed file describes the device platforms, connection detail, ports, credentials, etc. to represent the network topology being tested. The testbed files in this project are “testbed dev.yaml” and “testbed prod.yaml” corresponding to the dev and test topologies respectively.

#### 8.5.2. AEtest testscripts: 

[AEtest (Automation Easy Testing)](https://pubhub.devnetcloud.com/media/pyats/docs/aetest/introduction.html) is available as a standard component in pyATS in an effort to standardize the definition and execution of testcases & test scripts. These are one or more Python files used to define the setup, execution, and clean-up of individual tests. In this project there is a single AEtest file “network_testing/interface_errors/interface errors.py” to verify that no errors have been reported on network interfaces
in the testbed.

#### 8.5.3. Easypy job files: 

The Easypy job file combines the testbed that describes the network to be verified, and the test scripts to be run against the testbed. The job file runs the test scripts on the testbed and reports the results in a consumable fashion. “network_testing/interface_errors/interface_errors_job.py” is the Easypy job file.

The network test can be run with the following command where the job and testbed files are defined:

`pyats run job interface errors/interface errors job.py --testbed testbed dev.yaml`

The logs can be viewed using `pyats logs view`

<img src="/Images/pyats_logs.png" alt="Pyats Logs"/>

The setup and operation for network testing may be completely different than the one explained here if a tool other than pyATS is used.

### 8.6. NetDevOps CI/CD

The project uses NetDevOps concepts, and the files are stored in the GitLab public instance, where The configuration is version-controlled. The changes are made on this source of truth for device configurations giving rise to the concept of Continuous Integration (CI). The use of Ansible to apply configurations to network devices forms the basis for Network as Code (NaC). The changes are then applied using pipelines which is the basis for Continuous Delivery (CD).

To reiterate the workflow, when the code changes are committed locally and pushed to the GitLab server, the GitLab server detects the change and triggers the pipeline. The pipeline works first on the dev network and then on the prod network using the concept of branches. The pipeline is defined in the “.gitlab-ci.yml” file in the repository’s root directory. The commands in this file are run using GitLab Runner, which has network reachability to the network devices. The “.gitlab-ci.yml” contains the CI/CD configuration and the scripts to be run. It is also used to define the various stages of the pipeline.

#### 8.6.1. Branches

In Git, a branch is a new/separate version of the main repository. The project uses two branches, “dev” and “main”. The dev branch is used to deploy code on the dev network, while the “main” branch is used to deploy code on the prod network.

All the local code changes are pushed only to the dev branch and not the main branch. The dev branch runs the corresponding scripts to make configuration changes on the dev network first. After verifying that the configuration is pushed successfully, the dev branch can be “merged” with the main branch. When the merge is completed, the main branch is updated with all the code changes committed on the dev branch making the main and dev branches identical. The GitLab server then detects this change in the main branch, and now the scripts are run on the prod network.

#### 8.6.2. Pipeline

The scripts to be run are grouped into “jobs” which run as a part of the pipeline. The jobs can be grouped together into “stages” that run in a defined order. The stages and corresponsing jobs run in the order in which they are defined.

The jobs in stages ending with `-dev` run when the dev branch is active, while the jobs in stages ending with `-prod` run when the main branch is active. For the dev branch, we have the job `ping-devices-dev` (in line 14) in the stage of the same name `ping-devices-dev` (in line 15). The `script` keyword defines one or more commands run by the GitLab Runner, while the `tags` keyword is used to select the Runner.

An essential aspect to understand is the `only:refs` keyword. This keyword defines the active branch as the condition for the job to run. When the value of `only:refs` is `dev`, the job only runs when the dev branch is active. The same goes for the `ping-devices-prod` job and the main branch. The inventory files denoted by the `-i` flag are `hosts-dev` for the dev branch and `hosts-prod` for the prod branch. Since the dev and main branches use the same playbooks and data models, the pipeline works on the dev and prod inventories and, in turn, the topologies using this mechanism.

After the dev branch is merged with the main branch, the pipeline for the main branch is triggered. In case of a failure in any stage, the sequential natured pipeline stops.

## 9. Workflow Operation

As mentioned earlier, the playbooks here are only used to configure hostnames of the devices. This same setup can be modified to make more complex configuration changes on different models of devices. The aim here is to use this simple example to understand the NetDevOps process.

After forking the project and making the necessary adjustments, the pipeline runs the playbooks. The devices in dev and prod are configured to have hostnames "CiscoIOSXE-Dev" and "CiscoIOSXE-Prod" respectively.

<img src="/Images/old_hostname_dev.png" alt="Old Hostname Dev.png"/>

<img src="/Images/old_hostname_prod.png" alt="Old Hostname Prod.png"/>

The edit is first made to the dev branch in "/playbooks/cisco_ios_xe/hostname.yml" in line 8 by changing:

`lines: 'hostname {{ ansible_hostname }}'`

to

`lines: 'hostname {{ ansible_hostname_new }}'`

The pipeline runs in the dev branch changing the hostname to the following:

<img src="/Images/new_hostname_dev.png" alt="New Hostname Dev.png"/>

The dev branch can then be merged to the main branch and similar configuration change is applied to the main branch:

<img src="/Images/merge_request.png" alt="Merge Request.png"/>

<img src="/Images/new_hostname_prod.png" alt="New Hostname Prod.png"/>

Ideally there would be network testing after every configuration change in dev and prod.

## 10. Final Thoughts

NetDevOps sounds wonderful right? Why don't we adopt it rightaway then? 

Well, it's not that simple. The workflow model described here is not a copy-paste solution for you network configuration change problems. We have to realiatically consider the current scenario of the industry.

The first thing that comes to mind is the culture. As mentioned in the earlier sections, the culture of an SME performing manual configuration changes is engraved into the DNA of Network Operations. To shift towards automated code-based network changes, is not something that can or even should happen overnight. But maybe we can slowly make this shift by automating aspects of configuration changes, one by one. 

The ideal scenario of having identical dev and prod topologies may not be practical. Most companies use physical hardware as part of their network. Having an identical dev network with identical hardware is not a cost the company can bear. Using a virtual dev network may be a better solution to duplicate physical or even virtual prod networks. Then again there is cost associated with keeping this virtual dev network alive. With the rise of newer virtualization technologies, maybe creating and destroying dev environments for a particular network change is the way to go. 

An unsolvable problem is the use of devices with old firmware that don't support programmatic interfaces. It is a business decision if companies think automation is worth the cost of replacing old hardware.

## 11. References

H. Preston, “Part 1: Embrace netdevops, say goodbye to a “culture of fear”." [Online]. Available: https://blogs.cisco.com/developer/embrace-netdevops-part-1

H. Preston, “What does “network as code” mean?”.” [Online]. Available: https://blogs.cisco.com/developer/what-does-network-as-code-mean

A. Bednarz, “Top reasons for network downtime,”. [Online]. Available: https://www.networkworld.com/article/3142838/top-reasons-for-network-downtime.html

H. Preston, “It’s not a dream, netdevops cicd pipelines can develop, test, and deploy network configurations today,”. [Online]. Available: https://pubhub.devnetcloud.com/media/netdevops-live/site/files/s01t02.pdf

“What is version control?”. [Online]. Available: https://www.atlassian.com/git/tutorials/what-is-version-control

“git –fast-version-control,”. [Online]. Available: https://git-scm.com/

J. Shah, D. Dubaria, and J. Widhalm, “A survey of devops tools for networking,” in 2018 9th IEEE Annual Ubiquitous Computing, Electronics Mobile Communication Conference (UEMCON), 2018, pp. 185–188.

J. A. Shah and D. Dubaria, “Netdevops: A new era towards networking devops,” in 2019 IEEE 10th Annual Ubiquitous Computing, Electronics Mobile Communication Conference (UEMCON), 2019, pp. 0775–0779.

“Gitlab runner all tiers,” (accessed Feb. 28, 2022). [Online]. Available: https://docs.gitlab.com/runner/

H. Preston, “Introduction to writing network tests with pyats,”. [Online]. Available: https://www.slideshare.net/HankPreston1/introduction-to-writing-network-tests-with-pyats





